package main

import (
	"os"

	log "github.com/sirupsen/logrus"

	"gitlab.com/gitlab-org/security-products/analyzers/command/v3"
	"gitlab.com/gitlab-org/security-products/analyzers/security-code-scan/v3/metadata"
	"gitlab.com/gitlab-org/security-products/analyzers/security-code-scan/v3/plugin"
)

func main() {
	app := command.NewApp(metadata.AnalyzerDetails)

	app.Commands = command.NewCommands(command.Config{
		Match:        plugin.Match,
		Analyze:      analyze,
		AnalyzeAll:   true,
		Analyzer:     metadata.AnalyzerDetails,
		AnalyzeFlags: analyzeFlags(),
		Convert:      convert,
		Scanner:      metadata.ReportScanner,
		ScanType:     metadata.Type,
	})

	if err := app.Run(os.Args); err != nil {
		log.Fatal(err)
	}
}
